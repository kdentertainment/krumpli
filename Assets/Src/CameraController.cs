﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Vector3 cameraOffset = new Vector3(20f, 20f, -20f);

	Player3DController player;

	void Update () {
		MatchPlayerPosition ();
	}

	public void MatchPlayerPosition() {
		if (player != null) {
			Vector3 playerPosition = player.transform.position;

			transform.position = new Vector3(playerPosition.x, 0f, playerPosition.z) + cameraOffset;
		}
	}

	public Player3DController Player {
		get {
			return player;
		}
		set {
			player = value;
		}
	}
}
