﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject startMenu;
	public GameObject pauseMenu;

	GameObject levelPrefab;
	LevelController currentLevel;
	static bool isSoundOn = true;
	static bool gamePaused = false;
	static float masterVolume = 1f;
	static float pauseVolumeRate = 0.5f;


	void Awake () {
		levelPrefab = Resources.Load<GameObject> ("Prefabs/Level");
	}

	void Update () {
		if (Input.GetButton ("Cancel")) {
			if (!gamePaused) {
				PauseClicked ();
			}
		}
	}

	LevelController LoadNewLevel(LevelData levelData) {
		GameObject level = (GameObject)Instantiate (levelPrefab, Vector3.zero, Quaternion.identity);
		LevelController levelController = level.GetComponent<LevelController> ();
		levelController.Data = levelData;
		return levelController;
	}

	public void StartClicked () {
		if (currentLevel != null) {
			Destroy (currentLevel.gameObject);
		}

		currentLevel = LoadNewLevel (new LevelData ("Levels/01_01", 1));

		startMenu.SetActive (false);
	}

	public void PauseClicked() {
		pauseMenu.SetActive (true);
		GamePaused = true;
	}

	public void ContinueClicked() {
		pauseMenu.SetActive (false);
		GamePaused = false;
	}

	public void SoundToggled() {
		IsSoundOn = !IsSoundOn;
	}

	static void RefreshSoundSettings() {
		if (IsSoundOn) {
			if (GamePaused) {
				AudioListener.volume = pauseVolumeRate * masterVolume;
			} else {
				AudioListener.volume = masterVolume;
			}
		} else {
			AudioListener.volume = 0f;
		}
	}

	public void ExitClicked() {
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}

	public static bool GamePaused {
		get {
			return gamePaused;
		}
		set {
			gamePaused = value;
			RefreshSoundSettings ();
			Time.timeScale = value ? 0 : 1;
		}
	}

	public static bool IsSoundOn {
		get {
			return isSoundOn;
		}
		set {
			isSoundOn = value;
			RefreshSoundSettings ();
		}
	}
}
