﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData {

	public enum ElementType { Fix, Lift, Moving };

	public class Element {
		public string prefabName;
		public Vector3 position;
		public ElementType type;

		public List<Vector3> vectorData;
		public List<float> floatData;

		public Element(string n, Vector3 p, ElementType t, List<Vector3> v = null, List<float> f = null) {
			prefabName = n;
			position = p;
			type = t;

			vectorData = v;
			floatData = f;
		}
	}

	int floors;

	Color32 playerColor = new Color32 (255, 255, 255, 255);
	Color32 idleColor = new Color32 (0, 0, 255, 255);
	Color32 activeColor = new Color32 (255, 0, 0, 255);
	Color32 permanentColor = new Color32 (128, 128, 128, 255);
	Color32 groundColor = new Color32 (64, 64, 64, 255);
	Color32 goalColor = new Color32 (191, 191, 191, 255);
	Color32 movingStartColor = new Color32 (255, 128, 0, 255);
	Color32 movingPathColor = new Color32 (255, 192, 0, 255);

	public List<Element> elements;

	public Vector3 startingPosition;

	public LevelData(string s, int f) {
		floors = f;

		LoadFromPicture (s);
	}

	void LoadFromPicture(string path) {
		
		elements = new List<Element> ();

		Texture2D f = Resources.Load<Texture2D> (path + "_ground");
		Color32[] groundMap = f.GetPixels32 ();

		Color32[][] levelMaps = new Color32[3][];
		for (int i = 0; i < floors; ++i) {
			string p = path + "_lv" + i.ToString ();
			f = Resources.Load<Texture2D> (p);
			levelMaps[i] = f.GetPixels32 ();
		}

//		Color32[] collectMap = Resources.Load<Texture2D> (path + "_collectibles").GetPixels32 ();
//		Color32[] decorMap = Resources.Load<Texture2D> (path + "_decorations").GetPixels32 ();
//		Color32[] paramMap = Resources.Load<Texture2D> (path + "_eparams").GetPixels32 ();


		for (int x = 0; x < f.width; ++x) {
			for (int y = 0; y < f.height; ++y) {

				Color32 c = groundMap [y * f.width + x];
				Vector3 pos = new Vector3 (3f * x, 0f, 3f * y);

				if (ColorEquals(c, groundColor)) {
					elements.Add (new Element ("Ground", pos, ElementType.Fix));
				}

				for (int i = 0; i < floors; ++i) {
					bool hasStatic = false;

					c = levelMaps [i][y * f.width + x];
					pos = new Vector3 (3f * x, 2f * i, 3f * y);

					if (ColorEquals (c, playerColor)) {
						startingPosition = pos;
					} else if (ColorEquals (c, goalColor)) {
						elements.Add (new Element ("Goal", pos + new Vector3 (-0.5f, 0f, -0.5f), ElementType.Fix));
					} else if (ColorEquals (c, idleColor)) {
						elements.Add (new Element ("BlockIdle", pos, ElementType.Lift));
						if (i == 0) {
							hasStatic = true;
						}
					} else if (ColorEquals (c, activeColor)) {
						elements.Add (new Element ("BlockActive", pos, ElementType.Lift));
						if (i == 0) {
							hasStatic = true;
						}
					} else if (ColorEquals (c, permanentColor)) {
						elements.Add (new Element ("BlockPermanent", pos, ElementType.Fix));
						if (i == 0) {
							hasStatic = true;
						}
					} else if (ColorEquals (c, movingStartColor)) {
						List<Vector3> movingPath = FindPath (levelMaps[i], f.width, x, y, movingPathColor);

						elements.Add (new Element ("BlockMoving", pos, ElementType.Moving, movingPath));
						if (i == 0) {
							hasStatic = true;
						}
					}

					if (hasStatic) {
						elements.Add (new Element ("Ground", pos, ElementType.Fix));
					}
				}
			}
		}
	}

	List<Vector3> FindPath(Color32[] map, int w, int startX, int startY, Color32 c, List<Vector3> r = null, int dontCheck = -1) {
		if (r == null) {
			r = new List<Vector3> ();
		}

		int[,] d = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };

		for (int i = 0; i < 4; ++i) {
			if (i != dontCheck) {
				int x = startX + d [i, 0];
				int y = startY + d [i, 1];
				if (ColorEquals (c, map [y * w + x])) {
					r.Add (new Vector3 (3f * d [i, 0], 0f, 3f * d [i, 1]));
					r = FindPath (map, w, x, y, c, r, (i + 2) % 4);
				}
			}
		}

		return r;
	}

	bool ColorEquals(Color32 a, Color32 b) {
		return (a.a == b.a) && (a.r == b.r) && (a.b == b.b) && (a.g == b.g);
	}
}
