﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

	Dictionary<string, GameObject> prefabs;

	Player3DController playerController;

	CameraController cameraController;

	LevelData data;

	void Awake() {
		this.loadPrefabs ();
	}

	void Start () {
		this.CreateLevel (data);

		cameraController = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraController>();
		cameraController.Player = playerController;
		cameraController.MatchPlayerPosition ();

		playerController.RegisterDeath (OnPlayerDeath);
	}

	void Update () {
		
	}

	public void loadPrefabs() {
		GameObject[] prefabArray = Resources.LoadAll<GameObject> ("Prefabs");

		this.prefabs = new Dictionary<string, GameObject> ();

		foreach (var g in prefabArray) {
			prefabs.Add (g.name, g);
		}
	}

	public void CreateLevel (LevelData level){
		if (level == null) {
			level = new LevelData ("Levels/01_02", 3);
		}
		
		GameObject player = (GameObject)Instantiate (prefabs ["Player"], level.startingPosition, Quaternion.identity, this.transform);
		player.name = "Player";
		playerController = player.GetComponent<Player3DController> ();

		foreach (var e in level.elements) {
			GameObject block = (GameObject)Instantiate (prefabs [e.prefabName], e.position, Quaternion.identity, this.transform);
			block.name = e.prefabName + "_" + e.position.x + "_" + e.position.y;
			if (e.type == LevelData.ElementType.Lift) {
				BlockLiftController blockController = block.GetComponent<BlockLiftController> ();
				playerController.RegisterMovingChanged (blockController.OnMovingChanged);
			} else if (e.type == LevelData.ElementType.Moving) {
				BlockMovingController blockController = block.GetComponent<BlockMovingController> ();

				foreach (Vector3 v in e.vectorData) {
					blockController.deltaPosition += v;
				}

				playerController.RegisterMovingChanged (blockController.OnMovingChanged);
			}
		}
	}

	public void OnPlayerDeath() {
		playerController.Reset (data.startingPosition);
	}

	public LevelData Data {
		get {
			return data;
		}
		set {
			data = value;
		}
	}
}
