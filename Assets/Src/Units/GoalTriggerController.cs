﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTriggerController : TriggerController {

	GameObject player;

	public Vector3 targetOffset = new Vector3(-0.5f, 0f, -0.5f);
	public float goalSpeed = 2f;

	Action callbackPlayerFinished;

	public void FixedUpdate (){
		if (player != null) {
			Vector3 goalPosition = transform.parent.position;
			Vector3 playerPosition = player.transform.position;
			Vector3 target;

			if (playerPosition.x > goalPosition.x && playerPosition.z < (goalPosition.z - 0.5f)) {
				target = goalPosition + targetOffset - new Vector3 (0f, 0f, 0.5f);
			} else if (playerPosition.z > goalPosition.z && playerPosition.x < (goalPosition.x - 0.5f)) {
				target = goalPosition + targetOffset - new Vector3 (0.5f, 0f, 0f);
			} else {
				target = goalPosition + targetOffset;
			}

			Vector3 positionChange = (target - playerPosition).normalized * goalSpeed * Time.deltaTime;
			float distance = (target - playerPosition).magnitude;
			player.transform.position += Vector3.ClampMagnitude (positionChange, distance);
		}
	}

	public override void TriggerFired (GameObject triggerer) {
		if (triggerer.tag == "Player") {
			Player3DController playerController = triggerer.GetComponent<Player3DController> ();

			triggerer.GetComponent<CharacterController> ().enabled = false;
			playerController.enabled = false;

			player = triggerer;

			if (callbackPlayerFinished != null) {
				callbackPlayerFinished ();
			}
		}
	}

	public void RegisterPlayerFinished(Action cb) {
		this.callbackPlayerFinished += cb;
	}
}
