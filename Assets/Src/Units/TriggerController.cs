﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerController : MonoBehaviour {

	public abstract void TriggerFired (GameObject triggerer);
}
