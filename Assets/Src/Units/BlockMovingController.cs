﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMovingController : MonoBehaviour {

	Vector3 startingPosition;
	public Vector3 deltaPosition = Vector3.zero;
	public float changeDuration = 0.25f;

	float currentSpeed;
	float currentProgress;

	bool hasPlayer;
	bool playerMoving;

	Action callbackCarryingPlayer;

	void Awake () {
		startingPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);

		currentSpeed = 0;
		currentProgress = 0;

		hasPlayer = false;
		playerMoving = false;
	}

	void FixedUpdate () {
		currentProgress = Mathf.Clamp (currentProgress + currentSpeed * Time.deltaTime / changeDuration, 0f, 1f);

		transform.position = Vector3.Lerp (startingPosition, startingPosition + deltaPosition, currentProgress);
	}

	void RefreshState() {
		if (PlayerMoving && !HasPlayer) {
			currentSpeed = 1f;
		} else {
			currentSpeed = - 1f;

			if (HasPlayer && callbackCarryingPlayer != null && currentProgress != 0) {
				callbackCarryingPlayer ();
			}
		}
	}

	public void OnMovingChanged(bool m) {
		playerMoving = m;

		RefreshState ();
	}

	public void OnPlayerEnter (Player3DController p) {
		p.transform.parent = transform;

		HasPlayer = true;
	}

	public void OnPlayerExit (Player3DController p) {
		p.transform.parent = null;

		HasPlayer = false;
	}

	public bool HasPlayer {
		get {
			return hasPlayer;
		}
		set {
			if (hasPlayer != value) {
				hasPlayer = value;

				if (hasPlayer == false) {
					RefreshState ();
				}
			}
		}
	}

	public bool PlayerMoving {
		get {
			return playerMoving;
		}
		set {
			playerMoving = value;
		}
	}

	public void RegisterCarryingPlayer(Action cb) {
		callbackCarryingPlayer += cb;
	}
}
