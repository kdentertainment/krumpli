﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockLiftController : MonoBehaviour {

	public float startingHeight = 2.1f;
	public float endHeight = 0.1f;
	public float changeDuration = 0.25f;

	float changeSpeed;
	float currentSpeed;
	float currentHeight;

	BoxCollider blockCollider;
	Transform blockModel;
	Transform blockTrigger;

	bool hasPlayer;
	bool playerMoving;

	Action callbackCarryingPlayer;

	void Awake () {
		changeSpeed = (endHeight - startingHeight) / changeDuration;
		currentSpeed = 0;
		currentHeight = startingHeight;

		blockCollider = GetComponent<BoxCollider> ();
		blockModel = transform.Find ("Model").transform;
		blockTrigger = transform.Find ("Trigger").transform;

		hasPlayer = false;
		playerMoving = false;
	}

	void FixedUpdate () {
		float minHeight = Mathf.Min (startingHeight, endHeight);
		float maxHeight = Mathf.Max (startingHeight, endHeight);
		currentHeight = Mathf.Clamp (currentHeight + currentSpeed * Time.deltaTime, minHeight, maxHeight);

		blockCollider.center = new Vector3 (blockCollider.center.x, 0.5f * currentHeight - 0.1f, blockCollider.center.z);
		blockCollider.size = new Vector3 (blockCollider.size.x, currentHeight, blockCollider.size.z);

		blockModel.localPosition = new Vector3 (blockModel.localPosition.x, 0.5f * currentHeight - 0.1f, blockModel.localPosition.z);
		blockModel.localScale = new Vector3 (blockModel.localScale.x, currentHeight, blockModel.localScale.z);

		blockTrigger.localPosition = new Vector3 (blockTrigger.localPosition.x, currentHeight + 1.4f, blockTrigger.localPosition.z);
	}

	void RefreshState() {
		if (playerMoving && !HasPlayer) {
			currentSpeed = changeSpeed;
		} else {
			currentSpeed = - changeSpeed;

			if (HasPlayer && callbackCarryingPlayer != null && currentHeight != startingHeight) {
				callbackCarryingPlayer ();
			}
		}
	}

	public void OnMovingChanged(bool m) {
		playerMoving = m;

		RefreshState ();
	}

	public bool HasPlayer {
		get {
			return hasPlayer;
		}
		set {
			if (hasPlayer != value) {
				hasPlayer = value;

				if (hasPlayer == false) {
					RefreshState ();
				}
			}
		}
	}

	public void RegisterCarryingPlayer(Action cb) {
		callbackCarryingPlayer += cb;
	}
}
