﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player3DController : MonoBehaviour {

	public float speed = 1f;
	public float drag = 10f;

	public float deathSpeed = 1f;
	public float deathDepth = -5f;
	public float cutoffSpeed = 0f;

	public float bounceYMax = 0.5f;
	public float bounceStrength = 10f;

	Vector3 moveStrength;
	Vector3 velocity;

	#if UNITY_ANDROID
	Vector2 touchStart;
	float touchStrength = 0.02f;
	#endif

	CharacterController character;

	Action<bool> callbackMovingChanged;
	Action callbackDeath;
	Action callbackBounce;
	Action callbackSpawn;

	bool moving;

	void Awake() {
		character = GetComponent<CharacterController> ();
	}

	void Start () {
		moveStrength = Vector3.zero;
		velocity = Vector3.zero;

		moving = false;

		if (callbackSpawn != null) {
			callbackSpawn ();
		}
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {

		if (Mathf.Abs (hit.normal.y) < bounceYMax) {
			velocity += bounceStrength * new Vector3 (hit.normal.x, 0f, hit.normal.z).normalized;

			if (callbackBounce != null) {
				callbackBounce ();
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Lift") {
			other.gameObject.GetComponentInParent<BlockLiftController> ().HasPlayer = true;
		} else if (other.gameObject.tag == "Moving") {
			other.gameObject.GetComponentInParent<BlockMovingController> ().OnPlayerEnter (this);
		} else if (other.gameObject.tag == "Trigger") {
			other.gameObject.GetComponentInParent<TriggerController> ().TriggerFired (this.gameObject);
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Lift") {
			other.gameObject.GetComponentInParent<BlockLiftController> ().HasPlayer = false;
		} else if (other.gameObject.tag == "Moving") {
			other.gameObject.GetComponentInParent<BlockMovingController> ().OnPlayerExit (this);
		}
	}

	void Update () {
		if (character.isGrounded) {
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
			moveStrength = new Vector3 (Input.GetAxis ("Horizontal"), 0f, Input.GetAxis ("Vertical"));
			#endif
			#if UNITY_ANDROID
			if (Input.touchCount > 0) {
				Touch myTouch = Input.touches[0];

				if (myTouch.phase == TouchPhase.Began) {
					touchStart = myTouch.position;
				} else if (myTouch.phase == TouchPhase.Moved) {
					Vector2 touchDifference = Vector2.ClampMagnitude((myTouch.position - touchStart) * touchStrength, 1f);
					moveStrength = new Vector3(touchDifference.x, 0f, touchDifference.y);
				}
			} else {
				moveStrength = Vector3.zero;
			}
			#endif
		}
	}

	void FixedUpdate() {
		if (transform.position.y < deathDepth) {
			if (callbackDeath != null) {
				callbackDeath ();
			}
		}

		velocity += drag * moveStrength * speed * Time.deltaTime;

		character.SimpleMove (velocity);

		velocity = Vector3.Lerp (velocity, Vector3.zero, drag * Time.deltaTime);

		this.Moving = ((velocity.x * velocity.x + velocity.z * velocity.z) > cutoffSpeed * cutoffSpeed);
	}

	public void Reset(Vector3 pos) {
		transform.position = pos;

		Start ();
	}

	public bool Moving {
		get {
			return moving;
		}
		private set {
			if (value != moving && callbackMovingChanged != null) {
				callbackMovingChanged (value);
			}
			moving = value;
		}
	}

	public void RegisterMovingChanged(Action<bool> cb) {
		this.callbackMovingChanged += cb;
	}

	public void RegisterDeath(Action cb) {
		this.callbackDeath += cb;
	}

	public void RegisterBounce(Action cb) {
		this.callbackBounce += cb;
	}

	public void RegisterSpawn(Action cb) {
		this.callbackSpawn += cb;
	}
}
