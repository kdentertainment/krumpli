﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSoundController : MonoBehaviour {

	AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
		transform.GetComponentInParent<Player3DController> ().RegisterBounce (OnPlayerBounce);
	}

	void OnPlayerBounce() {
		source.Play ();
	}
}
