﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishMusicController : MonoBehaviour {

	AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
		transform.parent.GetComponentInChildren<GoalTriggerController> ().RegisterPlayerFinished (OnPlayerFinish);
	}

	void OnPlayerFinish() {
		source.Play ();
	}
}
