﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSoundController : MonoBehaviour {

	AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
		transform.GetComponentInParent<Player3DController> ().RegisterDeath (OnPlayerDeath);
	}

	void OnPlayerDeath() {
		source.Play ();
	}
}
