﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSoundController : MonoBehaviour {

	AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
		transform.GetComponentInParent<Player3DController> ().RegisterSpawn (OnPlayerSpawn);
	}

	void OnPlayerSpawn() {
		source.Play ();
	}
}
