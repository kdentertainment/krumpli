﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryingSoundController : MonoBehaviour {

	AudioSource source;

	void Awake () {
		source = GetComponent<AudioSource> ();
		transform.GetComponentInParent<BlockLiftController> ().RegisterCarryingPlayer (OnCarryingPlayer);
	}

	void OnCarryingPlayer() {
		source.Play ();
	}
}
