﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButtonController : MonoBehaviour {

	Button button;
	bool isThisOn;

	void Awake () {
		isThisOn = GameController.IsSoundOn;
		button = GetComponent<Button> ();
	}

	void Update() {
		if (isThisOn != GameController.IsSoundOn) {
			isThisOn = GameController.IsSoundOn;
			ToggleColor ();
		}
	}

	void ToggleColor() {
		ColorBlock colors = button.colors;

		if (isThisOn) {
			colors.normalColor = new Color32 (255, 255, 255, 255);
			colors.highlightedColor = new Color32 (77, 208, 225, 255);
		} else {
			colors.normalColor = new Color32 (255, 255, 255, 25);
			colors.highlightedColor = new Color32 (77, 208, 225, 25);
		}

		button.colors = colors;
	}
}
